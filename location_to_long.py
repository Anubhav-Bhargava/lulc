import sys
from geopy.geocoders import Nominatim

geolocator = Nominatim()

if 1==len(sys.argv):
	print "Please enter the location"
	exit()

#print sys.argv[1:]
try:
	location = geolocator.geocode(sys.argv[1:])

	#print(location.address)

	print(location.latitude, location.longitude)
except:
	print "Address not found."