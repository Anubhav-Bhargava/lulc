import sys
import numpy as np
import os
import urllib2
from geopy.geocoders import Nominatim
import getpass
from pymodis import downmodis
from requests.exceptions import ConnectionError


def get_coordinates(address):
	geolocator = Nominatim()
	try:
		location = geolocator.geocode(address)

		#print(location.address)
		return (location.latitude, location.longitude)
		print(location.latitude, location.longitude)
	except:
		print "If that address is correct, then please check your network connection"
		exit()


def get_tile(coordinates):
	data = np.genfromtxt('sn_bound_10deg.txt', 
                     skip_header = 7, 
                     skip_footer = 3)

	lat = coordinates[0]
	lon = coordinates[1]

	in_tile = False
	i = 0
	while(not in_tile):
	    in_tile = lat >= data[i, 4] and lat <= data[i, 5] and lon >= data[i, 2] and lon <= data[i, 3]
	    i += 1

	vert = data[i-1, 0]
	horiz = data[i-1, 1]
	return (vert, horiz)

def get_data(tiles):
	
	#print sys.argv[0]
	try:
		user=raw_input("Enter username : ")
		pswd = getpass.getpass('Password:')
		enddate=raw_input("Enter start date (yyyy.mm.dd) : ")	
		day=raw_input("Enter enddate (yyyy.mm.dd) : ")	
		product=raw_input("Enter product (Eg:- MOD13A2.006) : ")
		dest=raw_input("Destination Folder : ")

		modis_down = downmodis.downModis(destinationFolder=dest,user=user,
	                                   password=pswd, tiles=tiles, today=day, enddate=enddate, product=product)
		modis_down.connect()
		modis_down.downloadsAllDay()
	except:
		print "If those inputs are correct, then please check your network connection"
		exit()


if __name__ == '__main__':
	
	if 1==len(sys.argv):

		print "Please enter the location"
		exit()

	coordinates=get_coordinates(str(sys.argv[1:]))
	print coordinates

	tile_number=get_tile(coordinates)
	print tile_number

	modified_tile_number="h"+str(int(tile_number[1]))+"v0"+str(int(tile_number[0]))
	print modified_tile_number

	#os.system("modis_download.py ihi")
	get_data(modified_tile_number)
	print "Successfully downloaded"
