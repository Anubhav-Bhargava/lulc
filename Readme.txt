Project Title: Land Use and Land Cover Detection using MODIS data

Prerequisites:

> The code is developed for python 2.7

> All the dependencies required are mentioned in the Requirements.txt file

> To be able to download data you need user and password provided by NASA. Please register at https://urs.earthdata.nasa.gov/users/new; now login and move to your profile page. Go to “My application” tab and approve the following applications “LP DAAC Data Pool” and “Earthdata Search”.


Running:

The Package contains three scripts

1.location_to_long.py

This script generates longitude and latitude of the given address.
Run the following command in terminal:

python location_to_long.py your address

Output:
(latitude,longitude)

2.long_to_tile.py

This script generates modis tile numbers of the given latitude and longitude.
Run the following command in terminal:

python long_to_tile.py latitude,longitude

Output:
('Vertical Tile:', tile_number, 'Horizontal Tile:', tile_number)

3.modis_download.py

This script is used to download data from the NASA servers.
It has both GUI and command-line version. If wxPython is installed then the GUI version will run. 

Command line options

	-h  --help        shows the help message and exit
	-u  --url         http/ftp server url [default=https://e4ftl01.cr.usgs.gov]
	-I   --input      insert user and password from standard input
	-P  --password    password to connect
	-U  --username    username to connect
	-t  --tiles       string of tiles separated by comma
		          [default=none] for all tiles
	-s  --source      directory on the http/ftp server
		          [default=MOLT]
	-p  --product     product name as on the http/ftp server
		          [default=MOD11A1.005]
	-D  --delta       delta of day starting from first day [default=10]
	-f  --firstday    the day to start download, if you want change
		          data you have to use this format YYYY-MM-DD
		          ([default=none] is for today)
	-e  --endday      the day to finish download, if you want change
		          data you have to use this format YYYY-MM-DD
		          ([default=none] use delta option)
	-x                useful for debugging the download
		          [default=False]
	-j                download also the jpeg files [default=False]
	-O                download only one day, it sets delta=1 [default=False]
	-A                download all days, useful for initial download of a
		          product. It overwrites the 'firstday' and 'endday'
		          options [default=False]
	-r                remove files with size same to zero from
		          'destination_folder'  [default=False]

4. modis_convert.py

It converts MODIS data to TIF formats and different projection reference system. It is an interface to GDAL library.
Run the following command in terminal:

python modis_convert.py [options] hdf_file

Command line options

    -h, --help            show this help message and exit
    
    Required options:
      -s SUBSET, --subset=SUBSET
                          (Required) a subset of product's layers. The string
                          should be similar to: ( 1 0 )
      -o OUTPUT_FILE, --output=OUTPUT_FILE
                          (Required) the prefix of output file
      -g RESOLUTION, --grain=RESOLUTION
                          the spatial resolution of output file
      -r RESAMPLING_TYPE, --resampl=RESAMPLING_TYPE
                          the method of resampling. -- mrt methods:
                          'NEAREST_NEIGHBOR', 'BICUBIC', 'CUBIC_CONVOLUTION',
                          'NONE' -- gdal methods: 'AVERAGE', 'BILINEAR',
                          'CUBIC', 'CUBIC_SPLINE', 'LANCZOS', 'MODE',
                          'NEAREST_NEIGHBOR' [default=NEAREST_NEIGHBOR]
    
    Options for GDAL:
      -f OUTPUT_FORMAT, --output-format=OUTPUT_FORMAT
                          output format supported by GDAL [default=GTiff]
      -e EPSG, --epsg=EPSG
                          EPSG code for the output
      -w WKT, --wkt_file=WKT
                          file or string containing projection definition in WKT
                          format
      -v, --vrt           Read from a GDAL VRT file.
      --formats           print supported GDAL formats

5. all_in_one.py
    
    It is combination of all the above scripts. Just provide the address and it'll download the data.
    Run the following command in terminal:
    
    python all_in_one.py address
    





