import sys
import numpy as np
import urllib2

#wget https://modis-land.gsfc.nasa.gov/pdf/sn_bound_10deg.txt

data = np.genfromtxt('sn_bound_10deg.txt', 
                     skip_header = 7, 
                     skip_footer = 3)
if 1 == len(sys.argv):
    print "Please enter latitude and longitude"
    exit()

lat = float(sys.argv[1])
lon = float(sys.argv[2])

#lon = 28.524325
#lat =   77.1566009808315



in_tile = False
i = 0
while(not in_tile):
    in_tile = lat >= data[i, 4] and lat <= data[i, 5] and lon >= data[i, 2] and lon <= data[i, 3]
    i += 1

vert = data[i-1, 0]
horiz = data[i-1, 1]
print('Vertical Tile:', vert, 'Horizontal Tile:', horiz)

