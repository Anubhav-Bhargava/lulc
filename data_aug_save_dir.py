from PIL import Image
import numpy as np
import os
import glob
import re
from keras.preprocessing.image import ImageDataGenerator
from keras.preprocessing import image


epochs=10

datagen = ImageDataGenerator(
    featurewise_center=True,
    featurewise_std_normalization=True,
    rotation_range=20,
    width_shift_range=0.2,
    height_shift_range=0.2,
    horizontal_flip=True,
    vertical_flip=True)


def read_img(location):
	path = os.path.abspath('.cnn.py') #absolute path of program
	path = re.sub('[a-zA-Z\s._]+$', '', path) #remove unintended file
	x_train = []
	y_train = []
	x_test = []
	y_test = []
	dirs = os.listdir(path+'Images/')
	label = 0
	for i in dirs:
		n = 0
		count = 0
		for pic in glob.glob(path+'Images/'+i+'/*.tif'):
			x = image.load_img(pic)
			x = image.img_to_array(x) # this is a Numpy array with shape (3, 150, 150)
			x = x.reshape((1,) + x.shape)

			j = 0
			for batch in datagen.flow(x, batch_size=1,
				save_to_dir=path+'Images/'+i+'/', save_prefix='cat', save_format='tif'):
				j =j+ 1
				if j > 5:
					break

			
		#print(count)
		label = label + 1
	#return np.array(x_train),np.array(y_train),np.array(x_test),np.array(y_test)
read_img('Images/')
'''img_rows = 256
img_cols = 256
num_class = 21
x_train,y_train,x_test,y_test = read_img('Images/')

x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 3)
x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 3)

input_shape = (img_rows, img_cols, 3)

x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255
y_train = keras.utils.to_categorical(y_train, 21)
y_test = keras.utils.to_categorical(y_test, 21)'''


'''datagen.fit(x_train)
print len(x_train)'''


